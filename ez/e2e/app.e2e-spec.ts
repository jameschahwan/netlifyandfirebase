import { EzPage } from './app.po';

describe('ez App', function() {
  let page: EzPage;

  beforeEach(() => {
    page = new EzPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
