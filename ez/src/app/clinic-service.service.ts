import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { Clincs, Clinc } from './clincs';
const {
  Stitch,
  RemoteMongoClient,
  AnonymousCredential
} = require('mongodb-stitch-browser-sdk');

@Injectable()
export class ClinicServiceService {
  public clincs: ReplaySubject<Clincs> = new ReplaySubject(1);
  private client = Stitch.initializeDefaultAppClient('netstest-rzdtr');
  private db = this.client.getServiceClient(RemoteMongoClient.factory, 'netsTest-atlas').db('netstest');
  constructor() {
    this.getClinics();
  }

  public getClinics() {
    this.db.collection('Clinics').find({}, { limit: 100 }).toArray()
      .then((results) => {
        console.log('results from clinics', results);
        let clin = new Clincs();
        clin.ClincArray = results;
        this.clincs.next(clin);
        console.log('tis clin : ', clin)
      });
  }
}

