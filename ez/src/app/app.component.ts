import { Component, OnInit } from '@angular/core';
import { Clincs } from './clincs';
import { ClinicServiceService } from './clinic-service.service';
const {
  Stitch,
  RemoteMongoClient,
  AnonymousCredential
} = require('mongodb-stitch-browser-sdk');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  clincService: ClinicServiceService;

  public clinics: Clincs;

  constructor(clincService: ClinicServiceService){
    this.clincService= clincService; 
  }

  ngOnInit() {
    console.log('on init')
    

    this.clincService.clincs.subscribe((x)=> {this.clinics = x;
    console.log('this be x:', x)},(err)=> {console.warn(err)});
  }



}
