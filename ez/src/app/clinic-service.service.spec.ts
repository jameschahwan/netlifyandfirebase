/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ClinicServiceService } from './clinic-service.service';

describe('ClinicServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClinicServiceService]
    });
  });

  it('should ...', inject([ClinicServiceService], (service: ClinicServiceService) => {
    expect(service).toBeTruthy();
  }));
});
